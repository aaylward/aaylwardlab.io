---

// Layout import — provides basic page elements: <head>, <nav>, <footer> etc.
import BaseLayout from '../layouts/BaseLayout.astro';

// Component Imports
import Hero from '../components/Hero.astro';
import Icon from '../components/Icon.astro';
import Pill from '../components/Pill.astro';

// Page section components
import Skills from '../components/Skills.astro';
import Publications from '../components/Publications.md';
import Projects from '../components/Projects.md';

// Full Astro Component Syntax:
// https://docs.astro.build/basics/astro-components/
---

<BaseLayout>
	<div class="stack gap-20 lg:gap-48">
		<div class="wrapper stack gap-8 lg:gap-20">
			<header class="hero">
				<Hero
					title="Anthony Aylward"
					tagline="Bioinformatics Data Scientist"
					location="San Diego, CA"
					align="start"
				>
				<div class="roles">
					<Pill><Icon icon="graduation-cap" size="1.33em" /> PhD, Bioinformatics</Pill>
					<Pill><Icon icon="code" size="1.33em" /> Python</Pill>
					<Pill><Icon icon="code" size="1.33em" /> Rust</Pill>
					<Pill><Icon icon="code" size="1.33em" /> R</Pill>
					<Pill><Icon icon="code" size="1.33em" /> JavaScript</Pill>
				</div>
				<div class="roles">
					<Pill><Icon icon="code" size="1.33em" /> Bash</Pill>
					<Pill><Icon icon="code" size="1.33em" /> SQL</Pill>
					<Pill><Icon icon="git-branch" size="1.33em" /> Git</Pill>
					<Pill><Icon icon="gitlab-logo-simple" size="1.33em" /> CI/CD</Pill>
					<Pill><Icon icon="amazon-logo" size="1.33em" /> AWS</Pill>
				</div>
				</Hero>

				<img
					alt="Anthony Aylward in a red hat"
					width="240"
					height="310"
					src={"/assets/aaylward-profile-square.png"}
				/>
			</header>

			<Skills />
		</div>

		<main class="wrapper about">
			<section class="section with-background">
				<header class="section-header stack wit gap-2 lg:gap-4">
					<h3>CV</h3>
					<p>Accomplished scientific project leader and data scientist with 11 years experience, specialized in plant biology.</p>
				</header>
			</section>
			<section>
				<h2 class="section-title">Education</h2>
				<div class="content">
					<h3 class="subsubsection-title">PhD Bioinformatics & Systems Biology</h3>
					<i>UC San Diego, 2013-2021</i>
					<h3 class="subsubsection-title">BS Mathematics</h3>
					<i>UC Santa Barbara, 2008-2012</i>
				</div>
			</section>
			<section>
				<h2 class="section-title">Experience</h2>
				<h3 class="subsection-title">Co-Founder</h3>
				<div class="content">
					<h4 class="subsubsection-title">Rare Flora</h4>
					<i>March 2024 - present</i>
					<br/>
					<p>
						<b>Early-stage startup</b> using plants to extract rare earth elements from soil via phytomining.
						<ul>
							<li>Researched and acquired high-performance GPU workstation for compute and bioinformatics operations.</li>
							<li>Compiled data from literature and laboratory for techno-economic analysis.</li>
						  <li>Performed R-based statistical analysis of metal uptake across 4 plant species</li>
							<li>Met with collaborators and prospective investors to communicate goals and timelines, and to advance our plant science project.</li>
						</ul>
					</p>
				</div>
				<h3 class="subsection-title">Eco Director</h3>
				<div class="content">
					<h4 class="subsubsection-title">Nucleate San Diego</h4>
					<i>August 2023 - present (part-time/volunteer)</i>
					<br/>
					<p><a href="https://nucleate.xyz/locations/san-diego-ca/"><b>Student-led startup accelerator</b></a> focused on supporting technologies spun out of academic labs. 
						<ul>
							<li>Spearheaded recruitment and support of "Eco" cohort of 3 sustainability-oriented startup teams, as well as recruitment of Nucleate SD leadership. First San Diego-local Eco cohort.</li>
							<li>Participated in planning and executing 5 Nucleate SD Activator program events, including workshops on commercialization, intellectual property, and other topics.</li>
						</ul>
					</p>
				</div>
				<h3 class="subsection-title">Bioinformatics Analyst III</h3>
				<div class="content">
					<h4 class="subsubsection-title">Salk Institute for Biological Studies</h4>
					<i>November 2021 - March 2024</i>
					<p>
						Michael lab, Plant Biology dept. Genomics and phenomics for the <a href="https://www.salk.edu/harnessing-plants-initiative/">Harnessing Plants Initiative</a>, a department-wide research effort <b>engineering crop and wetland plants</b> to sequester carbon dioxide.
					</p>
					<p>
						<b>Data Science</b>
						<ul>
							<li>Designed on-site computing equipment for planned lab operations, liased with IT dept to procure and install it.</li>
							<li>Maintained shared computing infrastructure, onboarded and upskilled 12 lab members.</li>
							<li>Managed operating systems, data collection, and output analysis of Oxford Nanopore and Pacific Biosciences DNA sequencing machines.</li>
							<li>Spearheaded lab adoption of software engineering practices (test-driven development, documentation, CI/CD, etc).</li>
							<li>Developed 3 software tools for lab research operations. Primarily backend code executing compute-intensive bioinformatics algorithms to analyze plant genomes.</li>
							<li>Used Python, Bash, R, Snakemake, Docker/Apptainer, AWS and standard tools for genome assembly, sequence alignment, variant calling, differential gene expression, visualization with ggplot2 etc.</li>
							<li>Managed software projects using Git, CI/CD, test-driven development, and effective documentation (Sphinx etc.)</li>
						</ul>
						<b>Plant Biology</b>
						<ul>
							<li>Sequencing data QC and analysis of large-scale plant biology datasets, including genome assembly and epigenomic (e.g. DNA methylation) datasets of up to 200 samples, pangenome datasets of 1000+ samples, Large-scale GWAS datasets of e.g. 400 soybean samples, gene predictions, transcriptomic datasets including bulk and single-cell RNA-seq, gene networks, etc.</li>
							<li>Contributed scientific insights and writing to 6 publications.</li>
							<li>Contributed intensively to analysis of a GWAS experiment using image processing and ML to quantify root system architecture of soybean plants. Analysis included segmenting (<a href="https://github.com/MrGiovanni/UNetPlusPlus">UNet++</a>) and 2D/3D-skeletonizing methods (<a href="https://plantcv.danforthcenter.org/">PlantCV</a>). <a href="https://www.biorxiv.org/content/10.1101/2024.02.27.581071v1">Preprint</a>.</li>
							<li>Analazyed data generated from a variety of crops, including soybean, duckweed, cannabis, cassava, rice, maize, tomato, etc.</li>
							<li>Analyzed outputs from all major sequencing technnologies, including Illumina, Oxford Nanopore, PacBio, Hi-C</li>
							<li>Executed sequencing data analyses leading to identification of 7 quantitative trait-relevant target genes/alleles in rice and soybean.</li>
						</ul>
						<b>Leadership</b>
						<ul>
							<li>Served as senior member of a team of 5 bioinformaticians.</li>
							<li>Served as Co-chair of a DEI affinity group, led team with $6,000 budget to plan and execute 2-3 cultural enrichment events of 50-200 attendees annually.</li>
						</ul>
					</p>
				</div>
				<h3 class="subsection-title">Graduate Student</h3>
				<div class="content">
					<h4 class="subsubsection-title">University of California, San Diego</h4>
					<i>September 2013 - July 2021</i>
					<p>Gaulton lab, Pediatrics dept. <b>PhD student</b> in Bioinformatics & Systems Biology
						<ul>
							<li>Served as founding graduate student of a new lab, maintained shared computing infrastructure, trained and mentored 7 other graduate students, technicians, and undergraduates.</li>
							<li>Wrote dissertation on genomics and epigenomics of Type 1 and Type 2 diabetes in humans, including genomic and metabolomic analyses.</li>
							<li>Identified alleles influencing quantitative metabolic traits (e.g fasting glucose).</li>
							<li>Developed software tools for research pipelines that remain in use 3 years after departure from the lab.</li>
							<li>Designed, implemented, and hosted a Python web app to act as an information hub for graduate students in the Bioinformatics & Systems Biology program. Used the web framework <a href="https://flask.palletsprojects.com/en/3.0.x/">Flask</a>.</li>
						</ul>
					</p>
					</div>
			</section>
			<section>
				<h2 class="section-title">Selected Projects</h2>
				<div class="content">
					<Projects />
				</div>
			</section>
			<section>
			<h2 class="section-title">Publications</h2>
			<div class="content">
				<Publications />
			</div>
			</section>
		</main>
	</div>
</BaseLayout>

<style>
	.hero {
		display: flex;
		flex-direction: column;
		align-items: center;
		gap: 2rem;
	}

	.roles {
		display: none;
	}

	.hero img {
		aspect-ratio: 5 / 4;
		object-fit: cover;
		object-position: top;
		border-radius: 1.5rem;
		box-shadow: var(--shadow-md);
	}

	.about {
		display: flex;
		flex-direction: column;
		gap: 3.5rem;
	}

	section {
		display: flex;
		flex-direction: column;
		gap: 0.5rem;
		color: var(--gray-200);
	}

	.section-title {
		grid-column-start: 1;
		font-size: var(--text-xl);
		color: var(--gray-0);
		text-align: end;
	}
	.subsection-title {
		grid-column-start: 1;
		font-size: var(--text-lg);
		color: var(--gray-0);
		text-align: end;
	}
	.subsubsection-title {
		font-size: var(--text-lg);
		color: var(--gray-0);
	}

	.content {
		grid-column: 2 / 4;
	}

	.content :global(a) {
		text-decoration: 1px solid underline transparent;
		text-underline-offset: 0.25em;
		transition: text-decoration-color var(--theme-transition);
	}

	.content :global(a:hover),
	.content :global(a:focus) {
		text-decoration-color: currentColor;
	}

	@media (min-width: 50em) {
		.hero {
			display: grid;
			grid-template-columns: 6fr 4fr;
			padding-inline: 2.5rem;
			gap: 3.75rem;
		}

		.roles {
			margin-top: 0.5rem;
			display: flex;
			gap: 0.5rem;
		}

		.hero img {
			aspect-ratio: 3 / 4;
			border-radius: 4.5rem;
			object-fit: cover;
		}

		.about {
			display: grid;
			grid-template-columns: 1fr 60% 1fr;
		}

		.about > :global(:first-child) {
			grid-column-start: 2;
		}

		section {
			display: contents;
			font-size: var(--text-lg);
		}

	}

	/* ====================================================== */

	.section {
		display: grid;
		gap: 2rem;
	}

	.with-background {
		position: relative;
	}

	.with-background::before {
		--hero-bg: var(--bg-image-subtle-2);

		content: '';
		position: absolute;
		pointer-events: none;
		left: 50%;
		width: 100vw;
		aspect-ratio: calc(2.25 / var(--bg-scale));
		top: 0;
		transform: translateY(-75%) translateX(-50%);
		background:
			url('/assets/backgrounds/noise.png') top center/220px repeat,
			var(--hero-bg) center center / var(--bg-gradient-size) no-repeat,
			var(--gray-999);
		background-blend-mode: overlay, normal, normal, normal;
		mix-blend-mode: var(--bg-blend-mode);
		z-index: -1;
	}

	.with-background.bg-variant::before {
		--hero-bg: var(--bg-image-subtle-1);
	}

	.section-header {
		justify-self: center;
		text-align: center;
		max-width: 50ch;
		font-size: var(--text-md);
		color: var(--gray-300);
	}

	.section-header h3 {
		font-size: var(--text-2xl);
	}

	@media (min-width: 50em) {
		.section {
			grid-template-columns: repeat(4, 1fr);
			grid-template-areas: 'header header header header' 'gallery gallery gallery gallery';
			gap: 5rem;
		}

		.section-header {
			grid-area: header;
			font-size: var(--text-lg);
		}

		.section-header h3 {
			font-size: var(--text-4xl);
		}
	}
</style>
