*A selection of my academic software projects.*

<br/>

- [PanKmer](https://salk-tm.gitlab.io/pankmer/): *k*-mer based and reference-free pangenome analysis. ([source code](https://gitlab.com/salk-tm/pankmer/))
- [FTSCursor](https://pypi.org/project/ftscursor/) and [Flask-FTSCursor](https://pypi.org/project/Flask-FTSCursor/): A Python sqlite3 cursor with extra methods to support FTS3/4/5, and Flask. (source code: [FTSCursor](https://gitlab.com/aaylward/ftscursor), [Flask-FTSCursor](https://gitlab.com/aaylward/flask-ftscursor))
- [PyGNA2](https://salk-tm.gitlab.io/pygna2/): An improved interface for the gene network analysis tool [PyGNA](https://pygna.readthedocs.io/en/latest/). ([source code](https://gitlab.com/salk-tm/pygna2))
- [exploreATACseq](https://gitlab.com/aaylward/exploreatacseq): Exploratory analysis of multiple ATAC-seq datasets

<br/>

*See more comprehensive lists at my [GitLab account](https://gitlab.com/users/aaylward/contributed) or my [legacy CV](https://gitlab.com/aaylward/aaylward-cv).*